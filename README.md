# Sonar

[![Build Status](https://img.shields.io/travis/toxicwolf/sonar/master.svg?style=flat-square)](https://travis-ci.org/toxicwolf/sonar)
[![crates.io](https://img.shields.io/crates/v/sonar.svg?style=flat-square)](https://crates.io/crates/sonar)
[![License](https://img.shields.io/crates/l/sonar.svg?style=flat-square)](https://crates.io/crates/sonar)

3D game engine written in Rust

[Documentation](https://toxicwolf.github.io/sonar)

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
sonar = "*"
```

and this to your crate root:

```rust
extern crate sonar;
```

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
